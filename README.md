# Geodesic Dome for 99

IN THE PROCESS OF BEING DOCUMENTED.

## What's the Deal with Domes?
Geodesic domes have always occupied a special place in the heart of MIT, and of the Media Lab in particular. They are deeply [solar punk](https://pinkcastle.productions/) in their modular archiecture and utopian ideals. Inspired by [Jake, Sara and Agnes' dodecadome](https://gitlab.cba.mit.edu/jakeread/dodecadome) from a previous [99F](https://docs.google.com/document/d/1HsdU3MGi3vtlB2OIukiFrpB3p9z2m6j6qDhQZCE4PzQ/edit?usp=sharing), we decided to build a new one for the first outdoors 99F. Because the dome would be outside, we decided to go bigger in size. 

## Designing the Dome
<img src="img/dome6.png" alt="scheme" width = "500px">

The dome was designed for robustness. We did some beam analysis to determine critical nodal loads that the dome would be submitted when assembled. As it was such a large structure, the own structure would need to be eventual scafolding supporting structures for the top nodes. The loads were multiplied with a factor of safety of 4 and the result was that the structure really didnt care about those loads. The overal safety factor was over 41. 

<img src="img/dome4.png" alt="scheme" width = "800px">

Most [geodesic domes are designed to](http://www.domerama.com/dome-basics/odd-frequency-geodesic-domes-and-flat-base-at-the-hemisphere/) have a flat base by using a perfect hemisphere. This one was designed to have an additional layer of triangles below the halfway point, giving it a more upright form.

<img src="img/dome3.png" alt="scheme" width = "800px">

 The final design was a 2V dome, 3.1 meters tall and 4.3 meters in diameter. 

## Fabricating the Dome
We cut the hub components on the Fab Light. 

<img src="img/fab2.png" alt="scheme" width = "800px">

and folded them (18 degrees!) on the handbrake. 

Then we cut the 2x4's down to size, and pre-drilled them with a 3D printed component.

<img src="img/drill.png" alt="scheme" width = "800px">
<img src="img/fab1.png" alt="scheme" width = "800px">

## Assembling the Dome
Assembly took around 7 hours, with 3-5 people working on it continuously. 
<img src="img/dome2.png" alt="scheme" width = "800px">
<img src="img/assemble_early.png" alt="scheme" width = "800px">
<img src="img/assemble_mid.png" alt="scheme" width = "800px">
<img src="img/dome5.png" alt="scheme" width = "800px">
<img src="img/dome8.jpeg" alt="scheme" width = "800px">
<img src="img/dome1.png" alt="scheme" width = "800px">
<img src="img/alfonsodome.png" alt="scheme" width = "800px">

## Partying in the Dome
The 99F Party explored themes of hope, renewal and integration with nature. 
<img src="img/dome10.jpg" alt="scheme" width = "800px">
<img src="img/dome7.jpeg" alt="scheme" width = "800px">
<img src="img/dome_party.jpg" alt="scheme" width = "800px">













